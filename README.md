# HackerOne Scripts

This is going to be a collection of scripts I make to automate HackerOne things using their GraphQL API. At the moment there's only one script though. :)

## `subscribe_to_programs.rb`

Iterates through all private bug bounty programs and subscribes to notifications (if not already subscribed). If you're interested in notifications for public programs and VDPs, see the comments in the script.

- Requirements: Ruby, `httparty` gem (`gem install httparty`)
- Usage: `./subscribe_to_programs.rb HACKER-ONE-X-AUTH-TOKEN`
